import { Component, OnInit } from '@angular/core';
import { Product } from '../product.model';
import { Model } from '../repository.model';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent {

  constructor(private model: Model) { }

  getProducts(): Product[] {
    return this.model.getProducts();
  }

  rowSelected(selectedProduct: Product) {
    console.log('selected product: ' + selectedProduct.name);
    this.model.addProductToSelected(selectedProduct);
  }

  // ngOnInit() {

  //   // Get list of products from backend service
  //   this.productService.getProductList()
  //     .subscribe(resp => {
  //       // do happy response stuff
  //       console.log('http status: ' + resp.status);
  //       if (resp.status === 200) {
  //         this.productList = resp.body;
  //         console.log(this.productList);
  //       }
  //     },
  //       error => {
  //         console.log('error: ' + error);
  //       });



  // }

}
