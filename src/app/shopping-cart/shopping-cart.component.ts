import { Component, OnInit } from '@angular/core';
import { Model } from '../repository.model';
import { Product } from '../product.model';
import { Order } from '../order.model';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent {

  totalPrice: number;

  constructor(private model: Model) { }

  getSelectedProducts(): Product[] {
    return this.model.getSelectedProducts();
  }

  getTotalPrice(): number {
    this.totalPrice = 0;
    const selectedProducts = this.getSelectedProducts();
    selectedProducts.forEach(product => this.totalPrice += product.price);
    return this.totalPrice;

  }

  isShoppingCartEmpty(): boolean {
    if (this.getSelectedProducts().length > 0) {
      return false;
    } else {
      return true;
    }
  }

  removeProductFromShoppingCart(index: number) {
    const selectedProducts = this.getSelectedProducts();
    console.log('index: ' + index);
    selectedProducts.splice(index, 1);
  }

  generateOrder() {
    const selectedProducts = this.getSelectedProducts();
    const newSelectedProducts = selectedProducts.slice(0, selectedProducts.length);
    console.log(newSelectedProducts);


    const order = new Order(newSelectedProducts, this.totalPrice);
    this.model.addOrder(order);

    this.resetShoppingCart();
  }

  private resetShoppingCart() {
    this.model.clearSelectedProducts();
  }



}
