import { Product } from './product.model';

export class Order {

    constructor(public products: Product[],
                public totalPrice: number,
                public id?: string) {}
}
