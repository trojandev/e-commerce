import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpErrorResponse, HttpResponseBase } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Product } from './product.model';
import { Order } from './order.model';


@Injectable()
export class ProductOrderService {

  private eCommerceServicesUrl = 'https://e-commerce-services.herokuapp.com';

  constructor(private http: HttpClient) {
  }

  getProductList(): Observable<HttpResponse<Product[]>> {
    return this.http.get<Product[]>(this.eCommerceServicesUrl + '/product', { observe: 'response' });
  }

  saveOrder(order: Order): Observable<HttpResponse<Order>> {
    return this.http.post<Order>(this.eCommerceServicesUrl + '/order', order, { observe: 'response' });
  }

  getOrderList(): Observable<HttpResponse<Order[]>> {
    return this.http.get<Order[]>(this.eCommerceServicesUrl + '/order', { observe: 'response' });
  }

}
