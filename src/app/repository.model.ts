import { Injectable, OnInit } from '@angular/core';
import { Product } from './product.model';
import { Order } from './order.model';
import { ProductOrderService } from './product-order.service';

@Injectable()
export class Model {

    products: Product[];
    selectedProducts: Product[];
    orders: Order[];

    constructor(private productOrderService: ProductOrderService) {
        console.log('in repository constructor');
        this.selectedProducts = new Array<Product>();
        this.orders = new Array<Order>();
        this.fetchDataFromDatabase();
    }

    getProducts(): Product[] {
        return this.products;
    }

    getSelectedProducts(): Product[] {
        return this.selectedProducts;
    }

    addProductToSelected(product: Product) {
        this.selectedProducts.push(product);
    }

    getOrders(): Order[] {
        return this.orders;
    }

    clearSelectedProducts() {
      console.log(this.selectedProducts);
      this.selectedProducts.splice(0, this.selectedProducts.length);
      console.log(this.selectedProducts);

    }

    addOrder(order: Order) {
        // Save order to local order collection
        console.log('Order added: ');
        console.log('products: ' + order.products);
        console.log('totalPrice: ' + order.totalPrice);
        this.orders.push(order);

        // Save order to the database
        this.productOrderService.saveOrder(order)
        .subscribe(resp => {
            // do happy response stuff
            console.log('http status: ' + resp.status);
            if (resp.status === 200) {
              console.log('Order successfully stored in the database.');
              order.id = resp.body.id;
            }
          },
            error => {
              console.log('error: ' + error);
            });

    }

    private fetchDataFromDatabase() {
        // Get Products
        this.productOrderService.getProductList()
        .subscribe(resp => {
            // do happy response stuff
            console.log('http status: ' + resp.status);
            if (resp.status === 200) {
              this.products = resp.body;
              console.log(resp.body);
            }
          },
            error => {
              console.log('error: ' + error);
            });

        // Get Orders
        this.fetchOrders();
    }

    private fetchOrders() {
      this.productOrderService.getOrderList()
      .subscribe(resp => {
          // do happy response stuff
          console.log('http status: ' + resp.status);
          if (resp.status === 200) {
            this.orders = resp.body;
            console.log(resp.body);
          }
        },
          error => {
            console.log('error: ' + error);
          });
    }
}
