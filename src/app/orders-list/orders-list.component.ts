import { Component, OnInit } from '@angular/core';
import { Model } from '../repository.model';
import { Order } from '../order.model';

@Component({
  selector: 'app-orders-list',
  templateUrl: './orders-list.component.html',
  styleUrls: ['./orders-list.component.css']
})
export class OrdersListComponent {

  constructor(private model: Model) { }

  getOrders(): Order[] {
    console.log('getting orders...');
    return this.model.getOrders();
  }

}
