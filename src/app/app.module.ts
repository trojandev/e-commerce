import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { ProductListComponent } from './product-list/product-list.component';
import { HttpClientModule } from '@angular/common/http';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { Model } from './repository.model';
import { OrdersListComponent } from './orders-list/orders-list.component';
import { ProductOrderService } from './product-order.service';


@NgModule({
  declarations: [
    AppComponent,
    ProductListComponent,
    ShoppingCartComponent,
    OrdersListComponent
  ],
  imports: [
    BrowserModule, HttpClientModule
  ],
  providers: [ProductOrderService, Model],
  bootstrap: [AppComponent]
})
export class AppModule { }
